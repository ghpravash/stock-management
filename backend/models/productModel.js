import { Sequelize } from "sequelize";
import db from "../config/database.js";

const { DataTypes } = Sequelize;

const Product = db.define('products',{
    title:{
        type: DataTypes.STRING
    },
    quantity:{
        type:DataTypes.DOUBLE
    },
    price:{
        type:DataTypes.DOUBLE
    },
    category:{
        type:DataTypes.ENUM('momo', 'cold drinks', 'burger', 'tea', 'coffee')
    }
},{
    freezeTableName: true
});

export default Product;